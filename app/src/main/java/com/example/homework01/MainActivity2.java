package com.example.homework01;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    TextView Name, Email, Password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        //getData from Main
        Name = findViewById(R.id.name);
        Email = findViewById(R.id.email);
        Password = findViewById(R.id.password);

        Intent intent = getIntent();
        String fullName = intent.getStringExtra("Name");
        String email = intent.getStringExtra("Email");
        String password = intent.getStringExtra("Password");

        Name.setText(fullName);
        Email.setText(email);
        Password.setText(password);

        Button passDataTargetReturnDataButton = findViewById(R.id.btnBack);
        passDataTargetReturnDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("message_return", "Please create a new User");
                setResult(RESULT_OK, intent);
                finish();
            }
        });

    }

}
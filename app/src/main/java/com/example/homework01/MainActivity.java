package com.example.homework01;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button btnCancel, btnsave;
    EditText Name,Email, Password,crmPassword;
    boolean isAllFieldsChecked = false;
    User user = new User();
    private final static int REQUEST_CODE_1 = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnsave = findViewById(R.id.btnSave);
        btnCancel = findViewById(R.id.btnCancel);

        Name = findViewById(R.id.name);
        Email = findViewById(R.id.email);
        Password = findViewById(R.id.password);
        crmPassword=findViewById(R.id.confirmPassword);


        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isAllFieldsChecked = CheckAllFields();
                if (isAllFieldsChecked) {
                    user.setName(Name.getText().toString());
                    user.setEmail(Email.getText().toString());
                    user.setPassword(Password.getText().toString());

                    //pass to DetailActivity
                    Intent intent = new Intent(MainActivity.this, MainActivity2.class);
                    intent.putExtra("Name", user.Name);
                    intent.putExtra("Email", user.Email);
                    intent.putExtra("Password", user.Password);
                    startActivityForResult(intent, REQUEST_CODE_1);
                }

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.this.finish();
                System.exit(0);
            }
        });

    }

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private boolean CheckAllFields() {
        if (Name.length() == 0) {
            Name.setError("This field is required");
            return false;
        }

        if (Email.getText().toString().equals("")) {
            Email.setError("Email is required");
            return false;
        }else if (!Email.getText().toString().matches((emailPattern))){
            Email.setError("Invalid Email!");
            return false;
        }
        if (Password.getText().toString().equals("")) {
            Password.setError("Password is required");
            return false;

        }if (Password.length() < 8) {
            Password.setError("Password must be minimum 8 characters");
            return false;
        }if (crmPassword.getText().toString().equals("")) {
            crmPassword.setError("ConfirmPassword is required ");
            return false;
        }if(!Password.getText().toString().equals(crmPassword.getText().toString())) {
            crmPassword.setError("Password not match ");
            return false;
        }

        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case REQUEST_CODE_1:
                if(resultCode == RESULT_OK)
                {

                    String messageReturn = data.getStringExtra("message_return");
                    Context context = getApplicationContext();
                    CharSequence text = messageReturn;
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                    if (Name != null && Email != null && Password != null && crmPassword != null) {
                        Name.setText("");
                        Email.setText("");
                        Password.setText("");
                        crmPassword.setText("");
                    }
                }

        }
    }
}